package scrapers

import (
	"io/ioutil"
	"net/http"

	"github.com/tidwall/gjson"
)

// Location contains the latitude & longitude
type Location struct {
	Lat float64
	Lng float64
}

// GetLocation returns a location based on the google maps API
func GetLocation(query string, apiKey string) (Location, bool) {
	res, err := http.Get("https://maps.google.com/maps/api/geocode/json?address=" + query + "&key=" + apiKey)

	if err != nil {
		return Location{0, 0}, false
	}

	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return Location{0, 0}, false
	}

	defer res.Body.Close()

	json := string(body)

	if !gjson.Get(json, "results.0.geometry.location").Exists() {
		return Location{0, 0}, false
	}

	location := Location{
		Lat: gjson.Get(json, "results.0.geometry.location.lat").Num,
		Lng: gjson.Get(json, "results.0.geometry.location.lng").Num,
	}

	return location, true
}
