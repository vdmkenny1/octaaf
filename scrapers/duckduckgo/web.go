package duckduckgo

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/PuerkitoBio/goquery"
	"github.com/tidwall/gjson"
)

const ddgoLiteURI = "https://duckduckgo.com/lite?k1=-1&q="

// Search searches on duckduck go & returns the first url
func Search(query string, nsfw bool) (string, bool) {
	if len(query) == 0 {
		return "", false
	}

	bangURI, isBang := bang(query)
	if isBang {
		return bangURI, true
	}

	url := ddgoLiteURI + query

	if nsfw {
		url += "&kp=-2"
	}

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)

	if err != nil {
		return fmt.Sprintf("error: %s", err.Error()), false
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36")

	res, err := client.Do(req)

	if err != nil {
		return "", false
	}

	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)

	if err != nil {
		return "", false
	}

	return doc.Find(".result-link").First().Attr("href")
}

// Detect if the query is a bang query, return the duckduckgo redirect
// Eg: !arch amdgpu
func bang(query string) (string, bool) {
	if len(query) > 1 && query[0] == '!' {
		resp, err := http.Get(ddgoLiteURI + query)
		if err != nil {
			return "", false
		}

		uri := resp.Request.URL.String()
		defer resp.Body.Close()
		return uri, true
	}
	return "", false
}

// What tries to explain something using duckduckgo's wikipedia api
func What(query string) (string, bool, error) {
	resp, err := http.Get(fmt.Sprintf("https://api.duckduckgo.com/?q=%v&format=json&no_html=1&skip_disambig=1", query))
	if err != nil {
		return "", false, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", false, err
	}

	result := gjson.Get(string(body), "AbstractText").String()

	if len(result) == 0 {
		return "", false, nil
	}
	return result, true, nil
}
